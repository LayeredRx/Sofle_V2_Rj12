# Sofle V2 RJ12

This repo contains the basic Sofle V2 keyboard modified to use the RJ12 connector.

SofleKeyboard was created by [Josef Adamcik](https://josef-adamcik.cz/).

### Build Guide:
The build guide and original keybeord can be found at the Designers [website](https://josef-adamcik.cz/).


### The differing parts form the original are:
- The Reset switch is changed to **Panasonic** part number **EVQ-PUL02K**, which is now located below the microcontroller at the top of the board.
- The TTRS connector is replaced with an RJ12 connector **Amphenol** part number **54601-906WPLF located** in place of the TTRS.
- Interconnect cable is any 6 conductor cable terminated with RJ12 connectors.


### Firmware
Sofle uses [QMK firmware](https://qmk.fm/)


![SofleKeyboard](Images/IMG_1.jpg)
![SofleKeyboard](Images/IMG_3.jpg)

### Reset switch

The switch can be placed above or below the board, the chosen part is thinner than the mx switch sockets.

![SofleKeyboard](Images/IMG_2.jpg)
### RJ12 connector

When placed above or below the board the connector seats into the ovalized holes and lines up with the mask outline. It will only fit one way and cannot be installed incorrectly.

![SofleKeyboard](Images/IMG_4.jpg)

### Surface mount reset switch

Reset switch located at the 4 tabs in the top center of the picture.

![SofleKeyboard](Images/IMG_5.jpg)

### Connector pinout

The conductors are configured as follows, it will also work with Serial configuration. No programming changes are needed from the stock Sofle firmware.

![SofleKeyboard](Images/IMG_6.jpg)

### Credits 
- Josef Adamcik : Original Design
- LayeredRx : RJ12 modification
